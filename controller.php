
		public function register_user()
	{
		$allData=json_decode(file_get_contents("php://input"));

		foreach ($allData as $key => $value)
		{

				$_POST[$key] = $value;
		}









		// if($_POST['country_code']=='0')
		// {
		// 	$_POST['country_code']='';
		// }
		// if($_POST['currency']=='0')
		// {
		// 	$_POST['currency']='';
		// }

		$data = array('success' => false, 'messages' => array());

		$this->form_validation->set_rules('email', 'email id', 'required|is_unique[login.email]');
		$this->form_validation->set_rules('email', 'email', 'required|is_unique[login.email]|valid_email',
        array('is_unique' => 'Email has already been taken','valid_email'=>'Please provide a valid email address.')
				);
		$this->form_validation->set_rules('country_code', 'country code', 'required');
		$this->form_validation->set_rules('mobile_no', 'mobile no', 'required');
		$this->form_validation->set_rules('firstname', 'first name', 'required');
		$this->form_validation->set_rules('lastname', 'last name', 'required');
		$this->form_validation->set_rules('date_of_birth', 'date of birth', 'required');
		$this->form_validation->set_rules('currency', 'currency', 'required');
		$this->form_validation->set_rules('fullname', 'fullname', 'required');
		$this->form_validation->set_rules('username', 'username', 'required|is_unique[login.username]',
				array('is_unique' => 'Username has already been taken')
				);
		$this->form_validation->set_rules('user_id', 'user id', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('confirm_password', 'confirm password', 'required|matches[password]');
		$this->form_validation->set_rules('t_pin', 't pin', 'required');
		$this->form_validation->set_rules('tre_pin', 'tre_pin', 'required');
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		$this->form_validation->set_rules('sponser_id', 'sponser_id', 'required');

	  //$countSponserId=$this->user_model->sponserCheck($_POST['sponser_id']);

		// if($countSponserId>=4){
		// 	$_POST['sponser_id']='';
		// 	$this->form_validation->set_rules('sponser_id', 'sponser_id', 'required',
		// 	array('required'=>'
		// 		a sponser id cannot be used more than four'));
		// }

		if ($this->form_validation->run())
		{

				$data['success'] = true;
				$user=array(
					'email'=>$this->input->post('email'),
					'sponser_id'=>$this->input->post('sponser_id'),
					'country_code'=>$this->input->post('country_code'),
					'mobile_no'=>$this->input->post('mobile_no'),
					'firstname'=>$this->input->post('firstname'),
					'lastname'=>$this->input->post('lastname'),
					'date_of_birth'=>$this->input->post('date_of_birth'),
					'currency'=>$this->input->post('currency'),
					'fullname'=>$this->input->post('fullname'),
					'username'=>$this->input->post('username'),
					'user_id'=>$this->input->post('user_id'),
					'password'=>md5($this->input->post('password')),
					't_pin'=>$this->input->post('t_pin'),
					'tre_pin'=>$this->input->post('tre_pin'),
					'created_at'=>date('Y-m-d h:i:a'),
					'updated_at'=>date('Y-m-d h:i:a'),
					'verify_status'=>md5(rand(1,100000)),
				);
				$this->user_model->register_user($user);
			//$this->sendEmail($user['verify_status'],$user['email']);
			//$this->session->set_flashdata('message', '<strong>Thank you!</strong>Your Mail has been sent successfully.</span>');

		}

		else {

			foreach ($_POST as $key => $value)
			{

				  $data['messages'][$key] = form_error($key);
			}
		}
		echo json_encode($data);

	}